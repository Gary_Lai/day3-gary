package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Klass> klasses;
    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.klasses = new ArrayList<>();
    }

    @Override
    public String introduce() {
        if (this.klasses.size() > 0) {
            StringBuilder introduce = new StringBuilder();
            introduce.append(super.introduce());
            introduce.append(" I am a teacher. I teach Class ");
            StringJoiner className = new StringJoiner(", ");
            for (Klass klass : this.klasses) {
                className.add(String.valueOf(klass.getNumber()));
            }
            introduce.append(className.toString());
            introduce.append(".");
            return introduce.toString();
        }
        return super.introduce() + " I am a teacher.";
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return this.klasses.contains(student.getKlass());
    }

    @Override
    public void sayKnow(Klass klass) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n", this.getName(), klass.getNumber(), klass.getLeader().getName());
    }
}

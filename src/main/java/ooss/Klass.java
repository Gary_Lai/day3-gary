package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {


    private List<Person> observers
            = new ArrayList<Person>();
    private int number;

    private Student leader;

    public Klass(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (student == null) return;
        if (!this.equals(student.getKlass())) {
            System.out.println("It is not one of us.");
            return;
        }
        this.leader = student;
        this.notifyAllObservers();
    }

    public boolean isLeader(Student student) {
        if (student == null) {
            return false;
        }
        return student.equals(this.leader);
    }

    public Student getLeader() {
        return leader;
    }

    public void attach(Person person) {
        observers.add(person);
    }

    private void notifyAllObservers() {
        for (Person observer : observers) {
            observer.sayKnow(this);
        }
    }
}

package ooss;

public class Student extends Person {

    public Klass getKlass() {
        return klass;
    }

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder introduce = new StringBuilder();
        introduce.append(super.introduce());
        introduce.append(" I am a student.");
        if (klass != null) {
            if (this.equals(klass.getLeader())) {
                introduce.append(String.format(" I am the leader of class %d.", this.klass.getNumber()));
            } else {
                introduce.append(String.format(" I am in class %d.", this.klass.getNumber()));
            }
        }
        return introduce.toString();
    }

    public boolean isIn(Klass klass) {
        if (this.klass == null) {
            return false;
        }
        return this.klass.equals(klass);
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    @Override
    public void sayKnow(Klass klass) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n", this.getName(), klass.getNumber(), klass.getLeader().getName());
    }
}

##  Daily Summary 

**Time: 2023/7/12**

**Author: 赖世龙**

---

**O (Objective):**   Today, I learned code review, Java Stream Api and Java object Oriented. The teachers have instructed git commit for many times, which has also exercised git practice.

**R (Reflective):**  Satisfied

**I (Interpretive):**  Through many code exercises, I have understood and used these ideas. And these ideas are very useful and improve the quality of my code.

**D (Decisional):**   I will use these ideas more in the future code programming to reduce the amount of code and improve the readability of the code.

